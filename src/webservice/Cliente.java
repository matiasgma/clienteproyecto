package webservice;
import java.rmi.RemoteException;
import org.apache.axis2.AxisFault;
import webservice.ServicioArticuloStub.AgregarArticulo;
import webservice.ServicioArticuloStub.AgregarArticuloResponse;
import webservice.ServicioArticuloStub.Articulo;
import webservice.ServicioArticuloStub.MostrarArticulo;
import webservice.ServicioArticuloStub.MostrarArticuloResponse;
import webservice.ServicioArticuloStub.ModificarArticulo;
import webservice.ServicioArticuloStub.ModificarArticuloResponse;
import webservice.ServicioArticuloStub.EliminarArticulo;
import webservice.ServicioArticuloStub.EliminarArticuloResponse;
/**
 * Clase Cliente que se conecta al cliente del WS
 * @author Mat�as
 * @version 1.0
 */
public class Cliente {

	public static void main(String[] args) throws AxisFault,

RemoteException {
// TODO Auto-generated method stub

		ServicioArticuloStub oStub = new ServicioArticuloStub();

// AGREGA

Articulo oArticulo = new Articulo();
oArticulo.setNombreArticulo("Furgon");
oArticulo.setDescripcionArticulo("Blanco");
oArticulo.setPrecioCompra("3.000.000");
oArticulo.setValorAprox("2.800.000");
oArticulo.setIdProveedor("450");
oArticulo.setNombreProveedor("KIA");
oArticulo.setDireccionProveedor("Manuel Montt");
oArticulo.setActivo("Si");
oArticulo.setIdBodega("36");
oArticulo.setNombreBodega("BodegaKIA");
AgregarArticulo oAgregarArticulo = new AgregarArticulo();
oAgregarArticulo.setOArticulo(oArticulo);
AgregarArticuloResponse objResponse = oStub.agregarArticulo(oAgregarArticulo);
System.out.println("Resultado "+objResponse.get_return()); 



//Modificar Articulo
Articulo articulo = new Articulo();
articulo.setNombreArticulo("Televisor");
articulo.setDescripcionArticulo("32 pulgadas");
articulo.setPrecioCompra("80.000");
articulo.setValorAprox("75.000");
articulo.setIdProveedor("21");
articulo.setNombreProveedor("Ripley");
articulo.setDireccionProveedor("Lynch");
articulo.setActivo("Si");
articulo.setIdBodega("45");
articulo.setNombreBodega("BodegaTV");

ModificarArticulo oModificarArticulo = new ModificarArticulo();
oModificarArticulo.setOArticulo(articulo);
oModificarArticulo.setIdArticulo(2);
ModificarArticuloResponse objRespond = oStub.modificarArticulo(oModificarArticulo);
System.out.println("Resultado "+objRespond.get_return());


//Muestra Articulo

MostrarArticulo oMostrarArticulo = new MostrarArticulo();
MostrarArticuloResponse objResponde = oStub.mostrarArticulo(oMostrarArticulo);
Articulo[] articulos = objResponde.get_return();
int length = articulos.length;
for (int i = 0; i < length; i++) {
System.out.println(articulos[i]);
System.out.println("Nombre Articulo: "+articulos[i].getNombreArticulo());
System.out.println("Descripci�n Articulo: "+articulos[i].getDescripcionArticulo());
System.out.println("Precio compra: "+articulos[i].getPrecioCompra());
System.out.println("Valor Aproximado: "+articulos[i].getValorAprox());
System.out.println("Id Proveedor: "+articulos[i].getIdProveedor());
System.out.println("Nombre Proveedor:  "+articulos[i].getNombreProveedor());
System.out.println("Direcci�n Proveedor: "+articulos[i].getDireccionProveedor());
System.out.println("Proveedor Activo:  "+articulos[i].getActivo());
System.out.println("Id Bodega:  "+articulos[i].getIdBodega());
System.out.println("Nombre Bodega:  "+articulos[i].getNombreBodega());

}



// Eliminar Articulo
EliminarArticulo oEliminarArticulo = new EliminarArticulo();
oEliminarArticulo.setIdArticulo(1);
EliminarArticuloResponse objRespons = oStub.eliminarArticulo(oEliminarArticulo);
System.out.println("Resultado "+objRespons.get_return());


}
}

