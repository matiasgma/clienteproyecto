
/**
 * ServicioArticuloCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.1  Built on : Aug 31, 2011 (12:22:40 CEST)
 */

    package webservice;

    /**
     *  ServicioArticuloCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ServicioArticuloCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ServicioArticuloCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ServicioArticuloCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for agregarArticulo method
            * override this method for handling normal response from agregarArticulo operation
            */
           public void receiveResultagregarArticulo(
                    webservice.ServicioArticuloStub.AgregarArticuloResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from agregarArticulo operation
           */
            public void receiveErroragregarArticulo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for eliminarArticulo method
            * override this method for handling normal response from eliminarArticulo operation
            */
           public void receiveResulteliminarArticulo(
                    webservice.ServicioArticuloStub.EliminarArticuloResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from eliminarArticulo operation
           */
            public void receiveErroreliminarArticulo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for mostrarArticulo method
            * override this method for handling normal response from mostrarArticulo operation
            */
           public void receiveResultmostrarArticulo(
                    webservice.ServicioArticuloStub.MostrarArticuloResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from mostrarArticulo operation
           */
            public void receiveErrormostrarArticulo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for modificarArticulo method
            * override this method for handling normal response from modificarArticulo operation
            */
           public void receiveResultmodificarArticulo(
                    webservice.ServicioArticuloStub.ModificarArticuloResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from modificarArticulo operation
           */
            public void receiveErrormodificarArticulo(java.lang.Exception e) {
            }
                


    }
    